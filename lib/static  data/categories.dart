class CategoriesModel {
  int? id;
  String? title;
  String? imageUrl;
  String? category;
  String? time;
  CategoriesModel(
      {this.id, this.title, this.imageUrl, this.category, this.time});
}
