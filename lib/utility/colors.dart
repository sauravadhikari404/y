import 'package:flutter/material.dart';

class ChooseColor {
  Color blueColor = const Color(0xff003893);
  Color redColor = const Color(0xffDC143C);
  Color pinkColor = const Color(0xffDC143C);
  Color bodyBackgroundColor = const Color(0xfff5f5f5);
  Color flagColor = const Color(0xff003893);
  Color white = const Color(0xffffffff);
  Color appBarColor = const Color(0xffF3F3F3);
  ChooseColor(int i);
}
